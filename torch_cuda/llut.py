
import torch
from torch import nn
from torch.autograd import Function
from torch.utils.cpp_extension import load
from torch.cuda.amp import custom_fwd, custom_bwd

import os
current_dir = os.path.dirname(os.path.abspath(__file__))
sources = ['llkernel_torch_cuda.cu']
sources = [os.path.abspath(os.path.join(current_dir, x)) for x in sources]

llut_cuda = load(name='llut_cuda',
                sources=sources,
                extra_cflags=['-O3'],
                extra_cuda_cflags=['-O3', '--ptxas-options=-v', '-lineinfo'],
                verbose=True)


def make_forward_function(clifford):

    class LLUTForwardFunction(Function):

        @staticmethod
        @custom_fwd(cast_inputs=torch.float32)
        def forward(ctx, coords):

            indices, weights, grads = llut_cuda.forward(coords, clifford)
            ctx.save_for_backward(grads)
            ctx.mark_non_differentiable(indices)

            return indices, weights

        @staticmethod
        @custom_bwd
        def backward(ctx, d_indices, d_weights):

            d_coords = torch.einsum('bq, bpq -> bp', d_weights, *ctx.saved_variables)

            return d_coords

    return LLUTForwardFunction


def make_fused_lookup_function(normalise):

    class LLUTFusedLookupFunction(Function):

        @staticmethod
        def forward(ctx, coords, lut):

            output, indices, weights, grads = llut_cuda.monokernel(lut, coords, normalise, True)
            ctx.save_for_backward(indices, weights, grads, lut)

            return output

        @staticmethod
        def backward(ctx, d_output):

            indices, weights, grads, lut = ctx.saved_variables

            d_output = d_output.contiguous()

            d_coords = llut_cuda.coords_grad(lut, indices, weights, d_output, grads, normalise)
            d_lut = llut_cuda.llut_grad(lut.shape[0], indices, weights, d_output, normalise)

            return d_coords, d_lut

    return LLUTFusedLookupFunction


NormalisedLLUTFunction = make_fused_lookup_function(True)
UnnormalisedLLUTFunction = make_fused_lookup_function(False)

OrdinaryLLUTForwardFunction = make_forward_function(False)
CliffordLLUTForwardFunction = make_forward_function(True)


def fused_llut(coords, lut, normalised=True):
    '''
    Perform a LatticeLUT lookup (compatible with autograd).

    Parameters
    ----------

    coords : float32 cuda tensor of shape (..., k * 8);
    lut : float32 cuda tensor of shape (llut_size, llut_dim);

    Constraints
    -----------

    llut_size should be a power of two (between 256 and 16777216)
    and llut_dim should be a multiple of 32.

    Output
    ------

    output : float32 cuda tensor of shape (..., k * llut_dim).
    '''

    flat_coords = coords.view(-1, 8)
    F = NormalisedLLUTFunction if normalised else UnnormalisedLLUTFunction
    flat_output = F.apply(coords, lut)
    unflat_output = flat_output.view(*(list(coords.shape)[:-1] + [-1]))

    return unflat_output


def check_llut_size(llut_size, clifford=False):

    power = (31 if clifford else 24)

    if (llut_size < 256) or (llut_size > (2**power)) or (llut_size & (llut_size - 1)):
        raise ValueError("llut_size must be a power of two between 2^8 and 2^%d inclusive" % power)


def check_llut_dim(llut_dim):

    if (llut_dim <= 0) or (llut_dim & 31):
        raise ValueError("llut_dim must be a positive multiple of 32")


def apply_llut(coords, normalised=True, clifford=False, llut_size=16777216):
    '''
    Converts coordinates into indices and weights.

    WARNING: **only** works on the GPU, so be sure to call .cuda() on your module.

    Args:
        coords (tensor of shape (k, 8)): array of coordinates

        normalised (bool, optional, default True): whether weights should sum to 1

        clifford (bool, optional, default False): whether the torus is a product of circles

        llut_size (int): number of memory locations
                        (must be a power of 2 no larger than 2^24)

    Returns:
        indices (tensor of shape (k, 128)): integer indices
        weights (tensor of shape (k, 128)): floating-point weights
    '''

    if clifford:
        indices, weights = CliffordLLUTForwardFunction.apply(coords)
    else:
        indices, weights = OrdinaryLLUTForwardFunction.apply(coords)

    if normalised:
        weights = weights / torch.sum(weights, axis=1, keepdim=True)

    if llut_size is not None:
        # reduce modulo power of 2
        check_llut_size(llut_size, clifford)
        indices = torch.tensor([llut_size - 1], dtype=torch.int64, device=indices.device) & indices

    return indices, weights


def GradientCheck(n_points=20, device='cuda'):

    torch.manual_seed(42)

    ra64 = torch.randn(n_points, 8, dtype=torch.float64, device=device, requires_grad=True)
    passes_indexing = torch.autograd.gradcheck((lambda x : (apply_llut(x)[1])), ra64, eps=1.0e-6)

    ra32 = torch.randn(n_points, 8, dtype=torch.float32, device=device, requires_grad=True)
    ll = torch.randn(256, 32, device=device, requires_grad=True)
    passes_normalised   = torch.autograd.gradcheck(  NormalisedLLUTFunction.apply, (ra32, ll), eps=0.001, atol=0.001)
    passes_unnormalised = torch.autograd.gradcheck(UnnormalisedLLUTFunction.apply, (ra32, ll), eps=0.001, atol=0.001)

    return (passes_indexing and passes_normalised and passes_unnormalised)


class LatticeLUT(nn.Module):
    '''
    A fully differentiable eight-dimensional lookup table.

    WARNING: **only** works on the GPU, so be sure to call .cuda() on your module.

    Args:
        llut_size (int): number of memory locations
                        (must be a power of 2 no larger than 2^24)

        llut_dim (int): dimension of the float vector to store at each memory location
                        (must be divisible by 32)

        normalised (bool, optional, default True): whether weights should sum to 1

        **kwargs (associative parameter pack): parameters for nn.Embedding layer

    Shape:
        Input:  (..., k * 8) floating-point tensor
        Output: (..., k * llut_dim) floating-point tensor
    '''

    def __init__(self, llut_size, llut_dim, normalised=True, **kwargs):

        super(LatticeLUT, self).__init__()
        check_llut_size(llut_size)
        check_llut_dim(llut_dim)

        self.embedding  = nn.Embedding(llut_size, llut_dim, **kwargs)
        self.llut_size  = llut_size
        self.llut_dim   = llut_dim
        self.normalised = normalised

    def forward(self, coords):

        flat_coords = coords.view(-1, 8)

        if torch.is_grad_enabled() and (coords.requires_grad or self.embedding.weight.requires_grad):
            # Training mode:
            indices, weights = apply_llut(flat_coords, llut_size=self.llut_size, normalised=self.normalised)
            values = self.embedding(indices.view(-1)).view(indices.shape[0], indices.shape[1], self.llut_dim)
            vw = weights.view(weights.shape[0], weights.shape[1], 1) * values
            flat_output = vw.sum(axis=1)
        else:
            # Evaluation mode:
            # indices, weights = apply_llut(flat_coords, llut_size=None, normalised=False)
            # flat_output = llut_cuda.index_and_reduce(self.embedding.weight, indices, weights, self.normalised)
            (flat_output,) = llut_cuda.monokernel(self.embedding.weight, flat_coords, self.normalised, False)

        unflat_output = flat_output.view(*(list(coords.shape)[:-1] + [-1]))

        return unflat_output


def TrainingInferenceCheck(device='cuda'):

    rr = torch.randn(256, 8, device=device, requires_grad=True) * 10.0
    ll = LatticeLUT(65536, 32); ll.cuda(rr.device)

    def assertDifferenceBetween(x, y, lb, ub):

        t = float((x - y).abs().max())

        if (t < lb) or (t > ub):
            raise ValueError("float32 round-off should not be %s" % str(t))

    a = ll(rr)
    with torch.no_grad():
        b = ll(rr)

    ll.normalised = False

    c = ll(rr)
    with torch.no_grad():
        d = ll(rr)

    ll.normalised = True

    e = ll(rr)

    assertDifferenceBetween(a, b, 1.0e-12, 1.0e-5)
    assertDifferenceBetween(c, d, 1.0e-12, 1.0e-5)
    assertDifferenceBetween(a, c, 0.01, 10.0)
    assertDifferenceBetween(a, e, 0.0, 0.0)

    return True
