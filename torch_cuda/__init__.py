import torch
from .llut import LatticeLUT, GradientCheck, TrainingInferenceCheck, fused_llut, apply_llut
from math import pi

def harmonic_atan2(query):
    '''
    Converts a shape-(n, 16) query into a toroidal shape-(n, 8) query
    and return the reciprocal of the harmonic mean of the norms.
    '''

    q1, q2 = query[:, :8], query[:, 8:]
    inverse_norms = torch.rsqrt(torch.square(q1) + torch.square(q2) + 1.0e-8)
    query = torch.atan2(q1 * inverse_norms, q2 * inverse_norms)
    inverse_norms = inverse_norms.mean(1, keepdim=True)

    return query, inverse_norms


def apply_lram(query, memory_locations):
    '''
    Compute the (m, 128) indices and weights from an (m, 16) query vector
    as described in "Differentiable Random Access Memory using Lattices".
    These can be used in conjunction with an EmbeddingBag to implement the
    LRAM layer described in the paper.
    '''

    assert query.dim() == 2 and query.size(1) == 16
    assert (memory_locations & (memory_locations - 1)) == 0 # must be power of 2

    # apply atan2 to the query and remember the reciprocal scaling:
    query, inverse_norms = harmonic_atan2(query)

    # scale each coordinate from [-pi,pi] to the correct period:
    log2_indices = len(bin(memory_locations)) - 3
    dimensions = [(log2_indices >> 3) + (i < (log2_indices & 7)) for i in range(8)]
    clifford_scaling = [(2**d) / pi for d in dimensions]
    query = query * torch.tensor(clifford_scaling, dtype=torch.float32, device=query.device)

    # perform the E_8 lattice calculations:
    indices, weights = apply_llut(query, clifford=True, normalised=False, llut_size=memory_locations)

    # apply reciprocal scaling:
    weights = weights / inverse_norms.expand_as(weights)

    return indices, weights
