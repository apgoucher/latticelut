#include "../gpu/llkernel.h"
#include "../gpu/sparsemat.h"
#include "../gpu/coords_grad.h"
#include "common.h"

std::vector<torch::Tensor> llut_monokernel(torch::Tensor lut, torch::Tensor coords, bool normalised, bool needs_grad) {

    CHECK_INPUT(lut);
    auto lut_accessor     =     lut.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    CHECK_INPUT(coords);
    auto coords_accessor = coords.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    const at::cuda::OptionalCUDAGuard device_guard(device_of(coords));

    TORCH_CHECK(coords_accessor.size(1) == 8, "coords must have shape (N, 8)");
    int num_elements = coords_accessor.size(0);

    auto base_options = coords.options().requires_grad(false);

    auto indices = torch::empty({num_elements, 128}, base_options.dtype(torch::kInt32));
    auto weights = torch::empty({num_elements, 128}, base_options);

    auto indices_accessor = indices.packed_accessor32<int32_t,  2, torch::RestrictPtrTraits>();
    auto weights_accessor = weights.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    int llut_size = lut_accessor.size(0);
    int dimension = lut_accessor.size(1);

    auto output = torch::empty({num_elements, dimension}, base_options);
    auto output_accessor = output.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    if (needs_grad) {

        auto grads = torch::empty({num_elements, 8,128}, base_options);
        auto grads_accessor = grads.packed_accessor32<float, 3, torch::RestrictPtrTraits>();

        launch_monokernel(
            num_elements,
            coords_accessor.data(),
            coords_accessor.stride(0),
            coords_accessor.stride(1),
            indices_accessor.data(),
            weights_accessor.data(),
            llut_size,
            dimension,
            lut_accessor.data(),
            lut_accessor.stride(0),
            output_accessor.data(),
            output_accessor.stride(0),
            normalised,
            grads_accessor.data(),
            grads_accessor.stride(0),
            grads_accessor.stride(1),
            grads_accessor.stride(2)
        );

        return {output, indices, weights, grads};

    } else {

        launch_monokernel(
            num_elements,
            coords_accessor.data(),
            coords_accessor.stride(0),
            coords_accessor.stride(1),
            indices_accessor.data(),
            weights_accessor.data(),
            llut_size,
            dimension,
            lut_accessor.data(),
            lut_accessor.stride(0),
            output_accessor.data(),
            output_accessor.stride(0),
            normalised
        );

        return {output};

    }
}

torch::Tensor llut_index_and_reduce(torch::Tensor lut, torch::Tensor indices, torch::Tensor weights, bool renormalise) {

    CHECK_INPUT(lut);
    CHECK_INPUT(indices);
    CHECK_INPUT(weights);

    auto lut_accessor     =     lut.packed_accessor32<float, 2, torch::RestrictPtrTraits>();
    auto indices_accessor = indices.packed_accessor32<int32_t, 2, torch::RestrictPtrTraits>();
    auto weights_accessor = weights.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    int num_elements = indices_accessor.size(0);
    int llut_size = lut_accessor.size(0);
    int dimension = lut_accessor.size(1);

    auto base_options = weights.options().requires_grad(false);

    auto output = torch::empty({num_elements, dimension}, base_options);
    auto output_accessor = output.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    launch_lattice_lookup_kernel(
        num_elements,
        llut_size,
        dimension,
        indices_accessor.data(),
        indices_accessor.stride(0),
        indices_accessor.stride(1),
        weights_accessor.data(),
        weights_accessor.stride(0),
        weights_accessor.stride(1),
        lut_accessor.data(),
        lut_accessor.stride(0),
        output_accessor.data(),
        output_accessor.stride(0),
        output_accessor.stride(1),
        renormalise
    );

    return output;
}

template<typename scalar_t>
std::vector<torch::Tensor> llut_forward_template(torch::Tensor coords, bool needs_grad, bool clifford) {

    CHECK_INPUT(coords);

    const at::cuda::OptionalCUDAGuard device_guard(device_of(coords));

    auto coords_accessor = coords.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
    TORCH_CHECK(coords_accessor.size(1) == 8, "coords must have shape (N, 8)");
    int num_elements = coords_accessor.size(0);

    auto base_options = coords.options().requires_grad(false);

    auto indices = torch::empty({num_elements, 128}, base_options.dtype(torch::kInt32));
    auto weights = torch::empty({num_elements, 128}, base_options);

    auto indices_accessor = indices.packed_accessor32<int32_t,  2, torch::RestrictPtrTraits>();
    auto weights_accessor = weights.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();

    if (needs_grad) {

        auto grads = torch::empty({num_elements, 8,128}, base_options);
        auto grads_accessor     = grads.packed_accessor32<scalar_t, 3, torch::RestrictPtrTraits>();

        launch_lattice_lut_kernel<scalar_t>(
            clifford,
            num_elements,
            coords_accessor.data(),
            coords_accessor.stride(0),
            coords_accessor.stride(1),
            indices_accessor.data(),
            indices_accessor.stride(0),
            indices_accessor.stride(1),
            weights_accessor.data(),
            weights_accessor.stride(0),
            weights_accessor.stride(1),
            grads_accessor.data(),
            grads_accessor.stride(0),
            grads_accessor.stride(1),
            grads_accessor.stride(2)
        );

        return {indices, weights, grads};

    } else {

        launch_lattice_lut_kernel<scalar_t>(
            clifford,
            num_elements,
            coords_accessor.data(),
            coords_accessor.stride(0),
            coords_accessor.stride(1),
            indices_accessor.data(),
            indices_accessor.stride(0),
            indices_accessor.stride(1),
            weights_accessor.data(),
            weights_accessor.stride(0),
            weights_accessor.stride(1)
        );

        return {indices, weights};
    }

}

std::vector<torch::Tensor> llut_forward(torch::Tensor coords, bool clifford) {

    std::vector<torch::Tensor> return_tensors;

    AT_DISPATCH_FLOATING_TYPES(coords.type(), "llut_forward_template", ([&] {
        return_tensors = llut_forward_template<scalar_t>(coords, true, clifford);
    }));

    return return_tensors;
}

std::vector<torch::Tensor> llut_forward_nograd(torch::Tensor coords, bool clifford) {

    std::vector<torch::Tensor> return_tensors;

    AT_DISPATCH_FLOATING_TYPES(coords.type(), "llut_forward_template", ([&] {
        return_tensors = llut_forward_template<scalar_t>(coords, false, clifford);
    }));

    return return_tensors;
}

torch::Tensor llllut_grad(int llut_size, torch::Tensor indices, torch::Tensor weights, torch::Tensor d_output, bool renormalise) {

    auto d_output_accessor = d_output.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    CHECK_INPUT(indices);
    CHECK_INPUT(weights);
    CHECK_INPUT(d_output);

    auto indices_accessor  =  indices.packed_accessor32<int32_t,  2, torch::RestrictPtrTraits>();
    auto weights_accessor  =  weights.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    int dimension = d_output_accessor.size(1);
    int num_elements = indices_accessor.size(0);

    auto base_options = d_output.options().requires_grad(false);
    auto d_lut = torch::zeros({llut_size, dimension}, base_options);
    auto lut_accessor = d_lut.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    launch_lattice_write_kernel(
        num_elements,
        llut_size,
        dimension,
        indices_accessor.data(),
        indices_accessor.stride(0),
        indices_accessor.stride(1),
        weights_accessor.data(),
        weights_accessor.stride(0),
        weights_accessor.stride(1),
        lut_accessor.data(),
        lut_accessor.stride(0),
        d_output_accessor.data(),
        d_output_accessor.stride(0),
        d_output_accessor.stride(1),
        renormalise
    );

    return d_lut;
}

torch::Tensor llcoords_grad(torch::Tensor lut, torch::Tensor indices, torch::Tensor weights,
                            torch::Tensor d_output, torch::Tensor grads, bool renormalise) {

    auto d_output_accessor = d_output.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    CHECK_INPUT(lut);
    CHECK_INPUT(indices);
    CHECK_INPUT(weights);
    CHECK_INPUT(d_output);
    CHECK_INPUT(grads);

    auto lut_accessor      =      lut.packed_accessor32<float, 2, torch::RestrictPtrTraits>();
    auto indices_accessor  =  indices.packed_accessor32<int32_t,  2, torch::RestrictPtrTraits>();
    auto weights_accessor  =  weights.packed_accessor32<float, 2, torch::RestrictPtrTraits>();
    auto grads_accessor    =    grads.packed_accessor32<float, 3, torch::RestrictPtrTraits>();

    int llut_size = lut_accessor.size(0);
    int dimension = lut_accessor.size(1);
    int num_elements = indices_accessor.size(0);

    auto base_options = d_output.options().requires_grad(false);
    auto d_coords = torch::empty({num_elements, 8}, base_options);
    auto d_coords_accessor = d_coords.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

    launch_lattice_lut_coords_grad (
        num_elements,
        llut_size,
        dimension,
        indices_accessor.data(),
        weights_accessor.data(),
        lut_accessor.data(),
        lut_accessor.stride(0),
        d_output_accessor.data(),
        d_output_accessor.stride(0),
        grads_accessor.data(),
        grads_accessor.stride(0),
        d_coords_accessor.data(),
        renormalise
    );

    return d_coords;
}

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
    m.def("monokernel", &llut_monokernel, "LatticeLUT monokernel");
    m.def("forward", &llut_forward, "LatticeLUT forward");
    m.def("forward_nograd", &llut_forward_nograd, "LatticeLUT forward without gradients");
    m.def("index_and_reduce", &llut_index_and_reduce, "Compute weighted sum");
    m.def("coords_grad", &llcoords_grad, "Backpropagate gradient w.r.t. coords");
    m.def("llut_grad", &llllut_grad, "Backpropagate gradient w.r.t. LUT");
}
