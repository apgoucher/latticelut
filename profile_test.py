#!/home/apg44/.pyenv/versions/3.7.4/bin/python

if __name__ == '__main__':

    import torch_cuda, torch

    print('Gradient check...')
    assert(torch_cuda.GradientCheck())

    print('Training/inference check...')
    assert(torch_cuda.TrainingInferenceCheck())

    rr = torch.randn(65536, 8, device='cuda', requires_grad=True) * 10.0
    ll = torch_cuda.LatticeLUT(1048576, 32); ll.cuda()
    with torch.no_grad(): b = ll(rr); torch.cuda.synchronize()
