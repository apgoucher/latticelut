#pragma once

#include "../include/basics_gpu.h"
#define LLKERNEL_BLOCK_SIZE 32

template<bool renormalise, typename T>
_DI_ T ldwi (
        int element_id,
        int llut_size,
        const int32_t* __restrict__ indices,
        int indices_stride_N,
        int indices_stride_128,
        const T* __restrict__ weights,
        int weights_stride_N,
        int weights_stride_128,
        const T* __restrict__ llut,
        int llut_stride_L,
        T* __restrict__ s_weights,
        int32_t* __restrict__ s_indices
    ) {

    int lid = threadIdx.x;

    T w1 = weights[element_id * weights_stride_N + weights_stride_128 * lid];
    T w2 = weights[element_id * weights_stride_N + weights_stride_128 * (lid + 32)];
    T w3 = weights[element_id * weights_stride_N + weights_stride_128 * (lid + 64)];
    T w4 = weights[element_id * weights_stride_N + weights_stride_128 * (lid + 96)];

    #pragma unroll
    for (int i = 0; i < 4; i++) {
        int idx = indices[(lid + 32*i) * indices_stride_128 + element_id * indices_stride_N];
        s_indices[lid + 32*i] = ((idx & (llut_size - 1)) * (llut_stride_L >> 2));
    }

    T rw = 1;

    if (renormalise) {

        T w = w1 + w2 + w3 + w4;

        w += shuffle_xor_32(w, 1);
        w += shuffle_xor_32(w, 2);
        w += shuffle_xor_32(w, 4);
        w += shuffle_xor_32(w, 8);
        w += shuffle_xor_32(w, 16);

        rw /= w;

        w1 *= rw; w2 *= rw; w3 *= rw; w4 *= rw;
    }

    s_weights[lid] = w1;
    s_weights[lid + 32] = w2;
    s_weights[lid + 64] = w3;
    s_weights[lid + 96] = w4;

    __syncwarp();

    return rw; // reciprocal sum of unnormalised weights

}


template<bool renormalise>
_DI_ void lattice_lut_write_inner (
        int element_id,
        int llut_size,
        int dimension,
        const int32_t* __restrict__ indices,
        int indices_stride_N,
        int indices_stride_128,
        const float* __restrict__ weights,
        int weights_stride_N,
        int weights_stride_128,
        float* __restrict__ llut,
        int llut_stride_L,
        const float* __restrict__ output,
        int output_stride_N,
        int output_stride_D,
        float* __restrict__ s_weights,
        int32_t* __restrict__ s_indices
    ) {

    ldwi<renormalise>(element_id, llut_size,
        indices, indices_stride_N, indices_stride_128,
        weights, weights_stride_N, weights_stride_128,
        llut, llut_stride_L, s_weights, s_indices);

    __syncwarp();

    for (int t = threadIdx.x; t < dimension; t += 32) {

        float x = output[element_id * output_stride_N + t];

        for (int i = 0; i < 128; i += 4) {

            float w1 = s_weights[i];
            float w2 = s_weights[i+1];
            float w3 = s_weights[i+2];
            float w4 = s_weights[i+3];

            if (w1 == 0.0f) { break; }

            atomicAdd(&(llut[4 * s_indices[i]   + t]), w1 * x);
            atomicAdd(&(llut[4 * s_indices[i+1] + t]), w2 * x);
            atomicAdd(&(llut[4 * s_indices[i+2] + t]), w3 * x);
            atomicAdd(&(llut[4 * s_indices[i+3] + t]), w4 * x);
        }
    }
}


template<bool renormalise>
_DI_ void lattice_lut_lookup_inner (
        int element_id,
        int llut_size,
        int dimension,
        const int32_t* __restrict__ indices,
        int indices_stride_N,
        int indices_stride_128,
        const float* __restrict__ weights,
        int weights_stride_N,
        int weights_stride_128,
        const float* __restrict__ llut,
        int llut_stride_L,
        float* __restrict__ output,
        int output_stride_N,
        int output_stride_D,
        float* __restrict__ s_weights,
        int32_t* __restrict__ s_indices
    ) {

    ldwi<renormalise>(element_id, llut_size,
        indices, indices_stride_N, indices_stride_128,
        weights, weights_stride_N, weights_stride_128,
        llut, llut_stride_L, s_weights, s_indices);

    for (int t = threadIdx.x & 7; t < (dimension >> 2); t += 8) {

        float4 accs1;
        accs1.x = 0.0f; accs1.y = 0.0f; accs1.z = 0.0f; accs1.w = 0.0f;
        float4 accs2;
        accs2.x = 0.0f; accs2.y = 0.0f; accs2.z = 0.0f; accs2.w = 0.0f;

        auto llut2 = ((const float4*) (llut)) + t;

        #pragma unroll
        for (int i = 0; i < 16; i++) {

            int k = i * 8 + (threadIdx.x >> 3);
            float w1 = s_weights[k];

            if (i >= 7) { if (w1 == 0.0f) { break; } }

            float4 v1 = llut2[s_indices[k]];
            float4 v2 = llut2[s_indices[k+4]];

            float w2 = s_weights[k+4];

            accs1.x += w1 * v1.x;
            accs1.y += w1 * v1.y;
            accs1.z += w1 * v1.z;
            accs1.w += w1 * v1.w;

            accs2.x += w2 * v2.x;
            accs2.y += w2 * v2.y;
            accs2.z += w2 * v2.z;
            accs2.w += w2 * v2.w;
        }

        accs1.x += accs2.x;
        accs1.y += accs2.y;
        accs1.z += accs2.z;
        accs1.w += accs2.w;

        accs1.x += shuffle_xor_32(accs1.x, 8);
        accs1.y += shuffle_xor_32(accs1.y, 8);
        accs1.z += shuffle_xor_32(accs1.z, 8);
        accs1.w += shuffle_xor_32(accs1.w, 8);

        accs1.x += shuffle_xor_32(accs1.x, 16);
        accs1.y += shuffle_xor_32(accs1.y, 16);
        accs1.z += shuffle_xor_32(accs1.z, 16);
        accs1.w += shuffle_xor_32(accs1.w, 16);

        if (threadIdx.x < 8) {
            ((float4*) (output + (element_id * output_stride_N)))[t] = accs1;
        }
    }
}

template<bool renormalise>
__global__ void lattice_lut_lookup (
        int llut_size,
        int dimension,
        const int32_t* __restrict__ indices,
        int indices_stride_N,
        int indices_stride_128,
        const float* __restrict__ weights,
        int weights_stride_N,
        int weights_stride_128,
        const float* __restrict__ llut,
        int llut_stride_L,
        float* __restrict__ output,
        int output_stride_N,
        int output_stride_D
    ) {

    __shared__ float s_weights[128];
    __shared__ int32_t s_indices[128];

    lattice_lut_lookup_inner<renormalise>(blockIdx.x,
        llut_size,
        dimension,
        indices,
        indices_stride_N,
        indices_stride_128,
        weights,
        weights_stride_N,
        weights_stride_128,
        llut,
        llut_stride_L,
        output,
        output_stride_N,
        output_stride_D,
        s_weights,
        s_indices
    );
}

template<bool renormalise>
__global__ void lattice_lut_write (
        int llut_size,
        int dimension,
        const int32_t* __restrict__ indices,
        int indices_stride_N,
        int indices_stride_128,
        const float* __restrict__ weights,
        int weights_stride_N,
        int weights_stride_128,
        float* __restrict__ llut,
        int llut_stride_L,
        const float* __restrict__ output,
        int output_stride_N,
        int output_stride_D
    ) {

    __shared__ float s_weights[128];
    __shared__ int32_t s_indices[128];

    lattice_lut_write_inner<renormalise>(blockIdx.x,
        llut_size,
        dimension,
        indices,
        indices_stride_N,
        indices_stride_128,
        weights,
        weights_stride_N,
        weights_stride_128,
        llut,
        llut_stride_L,
        output,
        output_stride_N,
        output_stride_D,
        s_weights,
        s_indices
    );
}

void launch_lattice_lookup_kernel(
        int num_elements,
        int llut_size,
        int dimension,
        const int32_t* __restrict__ indices,
        int indices_stride_N,
        int indices_stride_128,
        const float* __restrict__ weights,
        int weights_stride_N,
        int weights_stride_128,
        const float* __restrict__ llut,
        int llut_stride_L,
        float* __restrict__ output,
        int output_stride_N,
        int output_stride_D,
        bool renormalise
    ) {

    if (renormalise) {
        lattice_lut_lookup<true><<<num_elements, 32>>>(llut_size, dimension,
            indices, indices_stride_N, indices_stride_128,
            weights, weights_stride_N, weights_stride_128,
            llut, llut_stride_L,
            output, output_stride_N, output_stride_D);
    } else {
        lattice_lut_lookup<false><<<num_elements, 32>>>(llut_size, dimension,
            indices, indices_stride_N, indices_stride_128,
            weights, weights_stride_N, weights_stride_128,
            llut, llut_stride_L,
            output, output_stride_N, output_stride_D);
    }

}

void launch_lattice_write_kernel(
        int num_elements,
        int llut_size,
        int dimension,
        const int32_t* __restrict__ indices,
        int indices_stride_N,
        int indices_stride_128,
        const float* __restrict__ weights,
        int weights_stride_N,
        int weights_stride_128,
        float* __restrict__ llut,
        int llut_stride_L,
        const float* __restrict__ output,
        int output_stride_N,
        int output_stride_D,
        bool renormalise
    ) {

    if (renormalise) {
        lattice_lut_write<true><<<num_elements, 32>>>(llut_size, dimension,
            indices, indices_stride_N, indices_stride_128,
            weights, weights_stride_N, weights_stride_128,
            llut, llut_stride_L,
            output, output_stride_N, output_stride_D);
    } else {
        lattice_lut_write<false><<<num_elements, 32>>>(llut_size, dimension,
            indices, indices_stride_N, indices_stride_128,
            weights, weights_stride_N, weights_stride_128,
            llut, llut_stride_L,
            output, output_stride_N, output_stride_D);
    }

}
