#pragma once
#include "sparsemat.h"

template<bool renormalise>
__global__ void lattice_lut_coords_grad (
        int llut_size,
        int dimension,
        const int32_t* __restrict__ indices,
        const float* __restrict__ weights,
        const float* __restrict__ llut,
        int llut_stride_L,
        const float* __restrict__ d_output,
        int d_output_stride_N,
        const float* __restrict__ grads,
        int grads_stride_N,
        float* __restrict__ d_coords
    ) {

    int element_id = blockIdx.x;

    __shared__ float dL_dx[128];
    __shared__ float s_weights[128];
    __shared__ int32_t s_indices[128];

    #pragma unroll
    for (int i = 0; i < 4; i++) {
        dL_dx[i*32 + threadIdx.x] = 0.0f;
    }

    float rw = ldwi<renormalise>(element_id, llut_size,
        indices, 128, 1, weights, 128, 1,
        llut, llut_stride_L, s_weights, s_indices);

    for (int i = 0; i < 4; i++) {

        if (s_weights[32*i] == 0.0f) { break; }

        float dp[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

        for (int t = threadIdx.x & 7; t < (dimension >> 2); t += 8) {

            float4 og = ((const float4*) (d_output + (element_id * d_output_stride_N)))[t];

            auto llut2 = ((const float4*) (llut)) + t;

            #pragma unroll
            for (int j = 0; j < 8; j++) {
                float4 v = llut2[s_indices[i*32 + j*4 + (threadIdx.x >> 3)]];
                dp[j] += v.x * og.x + v.y * og.y + v.z * og.z + v.w * og.w;
            }
        }

        #include "shuffle2.inc"

        dL_dx[i*32 + (threadIdx.x >> 3) + 4 * (threadIdx.x & 7)] = dp[0] * rw;
    }

    __syncwarp();

    if (renormalise) {
        float s = 0.0f;
        #pragma unroll
        for (int i = 0; i < 4; i++) {
            s += dL_dx[threadIdx.x + 32*i] * s_weights[threadIdx.x + 32*i];
        }
        s += shuffle_xor_32(s, 1);
        s += shuffle_xor_32(s, 2);
        s += shuffle_xor_32(s, 4);
        s += shuffle_xor_32(s, 8);
        s += shuffle_xor_32(s, 16);
        #pragma unroll
        for (int i = 0; i < 4; i++) {
            dL_dx[threadIdx.x + 32*i] -= s;
        }
    }

    __syncwarp();

    float l1 = dL_dx[threadIdx.x * 4];
    float l2 = dL_dx[threadIdx.x * 4 + 1];
    float l3 = dL_dx[threadIdx.x * 4 + 2];
    float l4 = dL_dx[threadIdx.x * 4 + 3];

    float dp[8];

    #pragma unroll
    for (int i = 0; i < 8; i++) {
        float4 lgrads = ((const float4*) &(grads[grads_stride_N * element_id + i * 128]))[threadIdx.x];
        dp[i] = l1 * lgrads.x + l2 * lgrads.y + l3 * lgrads.z + l4 * lgrads.w;
    }

    #include "shuffle2.inc"

    dp[0] += shuffle_xor_32(dp[0], 8);
    dp[0] += shuffle_xor_32(dp[0], 16);

    if (threadIdx.x < 8) {
        d_coords[element_id * 8 + threadIdx.x] = dp[0];
    }
}

void launch_lattice_lut_coords_grad (
        int num_elements,
        int llut_size,
        int dimension,
        const int32_t* __restrict__ indices,
        const float* __restrict__ weights,
        const float* __restrict__ llut,
        int llut_stride_L,
        const float* __restrict__ d_output,
        int d_output_stride_N,
        const float* __restrict__ grads,
        int grads_stride_N,
        float* __restrict__ d_coords,
        bool renormalise
    ) {

    if (renormalise) {
        lattice_lut_coords_grad<true><<<num_elements, 32>>>(llut_size,
            dimension, indices, weights, llut, llut_stride_L,
            d_output, d_output_stride_N, grads, grads_stride_N,
            d_coords);
    } else {
        lattice_lut_coords_grad<false><<<num_elements, 32>>>(llut_size,
            dimension, indices, weights, llut, llut_stride_L,
            d_output, d_output_stride_N, grads, grads_stride_N,
            d_coords);
    }

}
