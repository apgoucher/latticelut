template<typename T, bool normalised=true, bool clifford=false>
#ifdef USE_GRADIENTS
#ifdef FUSE_KERNELS
__global__ void lattice_lut_cuda_kernel_autograd_fused
#else
__global__ void lattice_lut_cuda_kernel_autograd
#endif
#else
#ifdef FUSE_KERNELS
__global__ void lattice_lut_cuda_kernel_fused
#else
__global__ void lattice_lut_cuda_kernel
#endif
#endif
    (
        int num_elements,
        const T* __restrict__ coords,
        int coords_stride_N,
        int coords_stride_8,
        int32_t* __restrict__ indices,
#ifndef FUSE_KERNELS
        int indices_stride_N,
        int indices_stride_128,
#endif
        T* __restrict__ weights
#ifndef FUSE_KERNELS
        , int weights_stride_N,
        int weights_stride_128
#endif
#ifdef USE_GRADIENTS
        , T* __restrict__ grads,
        int grads_stride_N,
        int grads_stride_8,
        int grads_stride_128
#endif
#ifdef FUSE_KERNELS
        , int llut_size,
        int dimension,
        const float* __restrict__ llut,
        int llut_stride_L,
        float* __restrict__ output,
        int output_stride_N
#endif
    ) {

    #ifdef FUSE_KERNELS
    int indices_stride_N = 128;
    int indices_stride_128 = 1;
    int weights_stride_N = 128;
    int weights_stride_128 = 1;
    int output_stride_D = 1;
    #endif

    __shared__ int32_t s_neighbours[256];

    // load neighbours into shared memory
    #pragma unroll
    for (int i = 0; i < 8; i++) {
        int j = (i << 5) + threadIdx.x;
        s_neighbours[j] = __232points_device[j];
    }

    __shared__ T underlying_smem_oc[8 * LLKERNEL_BLOCK_SIZE];
    TLSM<T> original_coordinates(underlying_smem_oc);

    int thread_offset = threadIdx.x + blockIdx.x * LLKERNEL_BLOCK_SIZE;

    // load coordinates
    #pragma unroll
    for (int i = 0; i < 8; i++) {
        T coord = 0.0;
        if (thread_offset < num_elements) {
            coord = coords[coords_stride_N * thread_offset + coords_stride_8 * i];
        }
        // Take advantage of the toroidal geometry to map the input into a
        // region where the floats are in [-16, 16]. This has two advantages:
        //  -- floating-point arithmetic is more precise near zero;
        //  -- we can use an int8_t to store the coordinates of the closest
        //     lattice point in shared memory, occupying less space.
        original_coordinates[i] = coord - ((T) 32.0) * std::round(coord * ((T) 0.03125));
    }

    __shared__ int8_t underlying_smem_lp[8 * LLKERNEL_BLOCK_SIZE];
    TLSM<int8_t> lattice_points(underlying_smem_lp);

    // find closest lattice point
    uint32_t tfm = find_closest_lattice_point(original_coordinates, lattice_points);

    __shared__ uint32_t transformations[LLKERNEL_BLOCK_SIZE];
    transformations[threadIdx.x] = tfm;

    __syncthreads();

    // do the expensive calculation

    for (int lane = 0; lane < 32; lane++) {

        int target_id = lane;
        uint32_t this_tfm = transformations[target_id];
        int this_centre[8];
        T this_point[8];

        #pragma unroll
        for (int i = 0; i < 8; i++) {
            this_centre[i] = underlying_smem_lp[target_id + i * LLKERNEL_BLOCK_SIZE];
            this_point[i]  = underlying_smem_oc[target_id + i * LLKERNEL_BLOCK_SIZE];
        }

        target_id += blockIdx.x * LLKERNEL_BLOCK_SIZE;

        if (target_id >= num_elements) {
            // handles situation where num_elements is not divisible by
            // LLKERNEL_BLOCK_SIZE;
            break;
        }

        int ones_before_word = 0;

        for (int sublane = 0; sublane < 8; sublane++) {

            int nidx = sublane * 32 + threadIdx.x;

            int neighbour[8];
            transform_neighbour(neighbour, this_centre, this_tfm, s_neighbours[nidx]);

            uint64_cu idx64 = (clifford) ? coords_to_uint64_clifford(neighbour) : coords_to_uint64(neighbour);
            int idx24 = mortonise(idx64) & 0x7fffffff;

            T delta[8];
            #pragma unroll
            for (int i = 0; i < 8; i++) {
                delta[i] = this_point[i] - neighbour[i];
            }

            T weight = weight_and_grad(delta);

            // Compute output memory location. We 'fill' the 256 virtual
            // memory locations from both ends, with the nonzero weights
            // starting from the left side and the zero weights starting
            // from the right side. Only the left 128 of these locations
            // are actually written:
            int mloc;
            {
                bool positive = weight > ((T) 0.0);
                uint32_t lanemask = ballot_32(positive);
                mloc = ones_before_word + __popc(lanemask & ((1u << threadIdx.x) - 1));
                ones_before_word += __popc(lanemask);

                if (!positive) {
                    mloc += 255 - nidx;
                    idx24 = -1;
                }
            }

            // save to GPU memory:
            if (mloc < 128) {
                indices[indices_stride_N * target_id + indices_stride_128 * mloc] = idx24;
                weights[weights_stride_N * target_id + weights_stride_128 * mloc] = weight;
                #ifdef USE_GRADIENTS
                if (grads != 0) {
                    #pragma unroll
                    for (int i = 0; i < 8; i++) {
                        grads[grads_stride_N * target_id + grads_stride_128 * mloc + i * grads_stride_8] = delta[i];
                    }
                }
                #endif
            }
        }
    }

    #ifdef FUSE_KERNELS

    for (int i = 0; i < 32; i++) {

        int target_id = blockIdx.x * LLKERNEL_BLOCK_SIZE + i;

        if (target_id >= num_elements) { break; }

        __syncwarp();

        lattice_lut_lookup_inner<normalised>(target_id,
            llut_size,
            dimension,
            indices,
            indices_stride_N,
            indices_stride_128,
            weights,
            weights_stride_N,
            weights_stride_128,
            llut,
            llut_stride_L,
            output,
            output_stride_N,
            output_stride_D,
            underlying_smem_oc,
            s_neighbours
        );

    }

    #endif
}

