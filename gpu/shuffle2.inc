
        {
            // shuffling step:
            if (threadIdx.x & 4) {
                #pragma unroll
                for (int j = 0; j < 4; j++) {
                    float k = dp[j]; dp[j] = dp[j+4]; dp[j+4] = k;
                }
            }

            #pragma unroll
            for (int j = 0; j < 4; j++) {
                dp[j] += shuffle_xor_32(dp[j+4], 4);
            }

            if (threadIdx.x & 2) {
                #pragma unroll
                for (int j = 0; j < 2; j++) {
                    float k = dp[j]; dp[j] = dp[j+2]; dp[j+2] = k;
                }
            }

            #pragma unroll
            for (int j = 0; j < 2; j++) {
                dp[j] += shuffle_xor_32(dp[j+2], 2);
            }

            if (threadIdx.x & 1) {
                float k = dp[0]; dp[0] = dp[1]; dp[1] = k;
            }

            dp[0] += shuffle_xor_32(dp[1], 1);
        }
