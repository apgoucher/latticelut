#include "llkernel.h"

#include <iostream>
#include <chrono>
#include <unordered_map>

double get_maximum_derivative(int N, int32_t *indices, float *weights) {

    std::cerr << "Checking..." << std::endl;

    std::unordered_map<int32_t, double> um;
    std::unordered_map<int32_t, double> vm;

    double maxdiff = 0.0;
    double nonzero_weights = 0.0;

    for (int i = 0; i < N; i++) {
        um.clear();

        double total_weight = 0.0;
        int negative_idxs = 0;

        for (int j = 0; j < 128; j++) {
            int32_t idx = indices[i*128+j];

            if (idx == -1) {
                negative_idxs += 1;
                continue;
            }

            if ((idx < 0) || (idx >= 16777216)) {
                std::cerr << "Fatal error: map contains invalid index " << idx << std::endl;
                exit(1);
            }

            double    w = weights[i*128+j];
            if (w > 0.0) { nonzero_weights += 1; }

            um[idx] = w;
            total_weight += w;
        }

        if (((int) um.size()) != 128 - negative_idxs) {
            std::cerr << "Fatal error: map contains only " << um.size() << " distinct indices." << std::endl;
            exit(1);
        }

        if ((total_weight < 0.8512) || (total_weight > 1.0001)) {
            std::cerr << "Fatal error: invalid total weight " << total_weight << std::endl;
            exit(1);
        }

        if (i > 0) {

            double thisdiff = 0.0;

            for (auto it = um.begin(); it != um.end(); ++it) {
                double x = it->second - vm[it->first];
                if (thisdiff < x) { thisdiff = x; }
                if (thisdiff < -x) { thisdiff = -x; }
            }

            if (thisdiff > maxdiff) { maxdiff = thisdiff; }
        }

        um.swap(vm);
    }

    std::cerr << "Maximum realised jump: " << maxdiff << std::endl;

    std::cerr << "Average neighbours per query point: " << (nonzero_weights / N) << std::endl;

    return maxdiff;
}


void lipschitz_test(int N, int M=1) {

    // create small vector:
    double rv[8];
    {
        int x[8] = {315, 99, 771, 346, 779, 37, 458, 77};
        for (int i = 0; i < 8; i++) {
            rv[i] = ((double) x[i]) / N;
        }
    }

    // |rv| * lipschitz_constant:
    double maxjump = ((double) 864) / N;

    std::cerr << "Maximum allowed jump: " << maxjump << std::endl;

    float    *d_coords; cudaMalloc(    (void**)  &d_coords, N * 8 * sizeof(float));
    float    *h_coords; cudaMallocHost((void**)  &h_coords, N * 8 * sizeof(float));

    int32_t *d_indices; cudaMalloc(    (void**) &d_indices, N * 128 * sizeof(int32_t));
    int32_t *h_indices; cudaMallocHost((void**) &h_indices, N * 128 * sizeof(int32_t));

    float   *d_weights; cudaMalloc(    (void**) &d_weights, N * 128 * sizeof(float));
    float   *h_weights; cudaMallocHost((void**) &h_weights, N * 128 * sizeof(float));

    // create input data:
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < 8; j++) {
            h_coords[8*i + j] = rv[j] * ((double) i);
        }
    }

    cudaMemcpy(d_coords, h_coords, N * 8 * sizeof(float), cudaMemcpyHostToDevice);

    auto t1 = std::chrono::high_resolution_clock::now();

    // launch the kernel lots of times for timing purposes:
    for (int i = 0; i < M; i++) {
        launch_lattice_lut_kernel<float>(false, N, d_coords, 8, 1, d_indices, 128, 1, d_weights, 128, 1);
    }

    cudaMemcpy(h_indices, d_indices, N * 128 * sizeof(int32_t), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_weights, d_weights, N * 128 * sizeof(float), cudaMemcpyDeviceToHost);

    auto t2 = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();

    std::cerr << duration << " microseconds." << std::endl;

    float maxdiff = get_maximum_derivative(N, h_indices, h_weights);

    // free the memory we've mallocated:
    cudaFree(    (void*) d_coords);
    cudaFreeHost((void*) h_coords);
    cudaFree(    (void*) d_indices);
    cudaFreeHost((void*) h_indices);
    cudaFree(    (void*) d_weights);
    cudaFreeHost((void*) h_weights);

    if (maxdiff > maxjump + 0.000001) { exit(1); }

}


int main(int argc, char* argv[]) {

    if (argc != 3) {
        std::cerr << "Usage: ./llkernel POINTS_PER_KERNEL_LAUNCH NUMBER_OF_KERNEL_LAUNCHES" << std::endl;
        std::cerr << "(e.g. ./llkernel 1048576 100)" << std::endl;
        return 1;
    }

    int N = std::atoi(argv[1]);
    int M = std::atoi(argv[2]);

    lipschitz_test(N, M);

    return 0;

}
