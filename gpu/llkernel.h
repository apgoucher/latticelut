#pragma once

#include "../include/basics_gpu.h"
#include "../include/nearest.h"
#include "../include/e8coords.h"
#include "../include/splines.h"

const static __device__ uint32_t __232points_device[256] =
#include "../include/lut232.inc"

#include "sparsemat.h"

// Legrandian metaprogramming:
#include "llkernel_core.h"
#define FUSE_KERNELS
#include "llkernel_core.h"
#define USE_GRADIENTS
#include "llkernel_core.h"
#undef FUSE_KERNELS
#include "llkernel_core.h"
#undef USE_GRADIENTS

/**
 * Host-side wrapper for the CUDA kernel. This dispatches to either a
 * 'training-mode' kernel (which additionally computes partial derivatives)
 * ot an 'inference-mode' kernel (which is more computationally efficient)
 * depending on whether the last four parameters are provided.
 */
template<typename T>
void launch_lattice_lut_kernel(
        bool clifford,
        int num_elements,
        const T *coords,
        int coords_stride_N,
        int coords_stride_8,
        int32_t *indices,
        int indices_stride_N,
        int indices_stride_128,
        T *weights,
        int weights_stride_N,
        int weights_stride_128,
        T *grads = 0,
        int grads_stride_N = 0,
        int grads_stride_8 = 0,
        int grads_stride_128 = 0) {

    int num_blocks = (num_elements + LLKERNEL_BLOCK_SIZE - 1) / LLKERNEL_BLOCK_SIZE;

    if (clifford) {
        if (grads == 0) {
            lattice_lut_cuda_kernel<T, false, true><<<num_blocks, LLKERNEL_BLOCK_SIZE>>>(
                num_elements, coords, coords_stride_N, coords_stride_8,
                indices, indices_stride_N, indices_stride_128,
                weights, weights_stride_N, weights_stride_128);
        } else {
            lattice_lut_cuda_kernel_autograd<T, false, true><<<num_blocks, LLKERNEL_BLOCK_SIZE>>>(
                num_elements, coords, coords_stride_N, coords_stride_8,
                indices, indices_stride_N, indices_stride_128,
                weights, weights_stride_N, weights_stride_128,
                grads, grads_stride_N, grads_stride_8, grads_stride_128);
        }
    } else {
        if (grads == 0) {
            lattice_lut_cuda_kernel<T, false, false><<<num_blocks, LLKERNEL_BLOCK_SIZE>>>(
                num_elements, coords, coords_stride_N, coords_stride_8,
                indices, indices_stride_N, indices_stride_128,
                weights, weights_stride_N, weights_stride_128);
        } else {
            lattice_lut_cuda_kernel_autograd<T, false, false><<<num_blocks, LLKERNEL_BLOCK_SIZE>>>(
                num_elements, coords, coords_stride_N, coords_stride_8,
                indices, indices_stride_N, indices_stride_128,
                weights, weights_stride_N, weights_stride_128,
                grads, grads_stride_N, grads_stride_8, grads_stride_128);
        }
    }
}

void launch_monokernel(
        int num_elements,
        const float *coords,
        int coords_stride_N,
        int coords_stride_8,
        int32_t *indices,
        float *weights,
        int llut_size,
        int dimension,
        const float* llut,
        int llut_stride_L,
        float* output,
        int output_stride_N,
        bool normalise,
        float *grads = 0,
        int grads_stride_N = 0,
        int grads_stride_8 = 0,
        int grads_stride_128 = 0) {

    int num_blocks = (num_elements + LLKERNEL_BLOCK_SIZE - 1) / LLKERNEL_BLOCK_SIZE;

    if (grads == 0) {
        if (normalise) {
            lattice_lut_cuda_kernel_fused<float, true><<<num_blocks, LLKERNEL_BLOCK_SIZE>>>(
                num_elements, coords, coords_stride_N, coords_stride_8,
                indices, weights,
                llut_size, dimension, llut, llut_stride_L,
                output, output_stride_N);
        } else {
            lattice_lut_cuda_kernel_fused<float, false><<<num_blocks, LLKERNEL_BLOCK_SIZE>>>(
                num_elements, coords, coords_stride_N, coords_stride_8,
                indices, weights,
                llut_size, dimension, llut, llut_stride_L,
                output, output_stride_N);
        }
    } else {
        if (normalise) {
            lattice_lut_cuda_kernel_autograd_fused<float, true><<<num_blocks, LLKERNEL_BLOCK_SIZE>>>(
                num_elements, coords, coords_stride_N, coords_stride_8,
                indices, weights,
                grads, grads_stride_N, grads_stride_8, grads_stride_128,
                llut_size, dimension, llut, llut_stride_L,
                output, output_stride_N);
        } else {
            lattice_lut_cuda_kernel_autograd_fused<float, false><<<num_blocks, LLKERNEL_BLOCK_SIZE>>>(
                num_elements, coords, coords_stride_N, coords_stride_8,
                indices, weights,
                grads, grads_stride_N, grads_stride_8, grads_stride_128,
                llut_size, dimension, llut, llut_stride_L,
                output, output_stride_N);
        }
    }

}
