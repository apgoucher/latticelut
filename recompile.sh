#!/bin/bash

mkdir build 2>&1 | true

set -ex

cd build
cmake -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang -DCMAKE_BUILD_TYPE=Release ..
make -j "$( getconf _NPROCESSORS_ONLN )"
