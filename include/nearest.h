#pragma once
#include "basics.h"
#include <cmath>
#include <utility>


template<typename T, typename U>
_HD_ void transform_neighbour(U& output, const T& centre, const uint32_t& tfm, const uint32_t& offset) {

    #pragma unroll
    for (int i = 0; i < 8; i++) {
        uint32_t tfmi = tfm >> (4*i);
        int co = ((int) ((offset >> (4*(tfmi & 7))) & 15)) - 4;
        if (tfmi & 8) { co = -co; }
        output[i] = centre[i] + co;
    }
}


/**
 * Find the closest point x in the E_8 lattice to the specified point c,
 * and return an element of the hyperoctahedral group that maps c - x
 * into the polytope P.
 */
template<typename T, typename U>
_HD_ uint32_t find_closest_lattice_point(const U& c, T& x) {

    int tp = 0;
    int y[8];

    #pragma unroll
    for (int i = 0; i < 8; i++) {
        float ci = (float) c[i];
        int xi = 2 * ((int) std::roundf(ci * 0.5f));
        tp ^= xi;
        int diff = ((int) ((ci - xi) * 8388608.0f)) << 4;
        if (diff < 0) { diff = 8 - diff; }
        y[i] = diff + i;
        x[i] = xi;
    }

    // sort descending using the optimal sorting network for 8 values:
    {
        #define CSWAP(i, j) inplace_maxmin(y[i], y[j])

        CSWAP(0, 1);
        CSWAP(2, 3);
        CSWAP(4, 5);
        CSWAP(6, 7);

        CSWAP(0, 2);
        CSWAP(1, 3);
        CSWAP(4, 6);
        CSWAP(5, 7);

        CSWAP(1, 2);
        CSWAP(5, 6);

        CSWAP(0, 4);
        CSWAP(1, 5);
        CSWAP(2, 6);
        CSWAP(3, 7);

        CSWAP(2, 4);
        CSWAP(3, 5);

        CSWAP(1, 2);
        CSWAP(3, 4);
        CSWAP(5, 6);

        #undef CSWAP
    }

    if (tp & 2) {
        // We've rounded to an odd-parity point. Fix this:
        x[y[0] & 7] += ((y[0] & 8) ? -2 : 2);
        y[0] ^= 0x0ffffff8;
    }

    uint32_t perm = 0;

    #pragma unroll
    for (int i = 0; i < 8; i++) {
        perm |= ((uint32_t) (i + (y[i] & 8))) << (4 * (y[i] & 7));
    }

    int coordsum = y[0] + y[1] + y[2] + y[3] + y[4] + y[5] + y[6];

    {
        uint32_t pp = perm; pp ^= (pp >> 16); pp ^= (pp >> 8); pp ^= (pp >> 4);
        if (pp & 8) {
            // We've negated an odd number of coordinates. Fix this:
            perm ^= (8 << (4 * (y[7] & 7)));
            coordsum -= y[7];
        } else {
            coordsum += y[7];
        }
    }

    if (coordsum >= 0x20000000) {
        // We're closer to a half-integer point. Fix this:
        #pragma unroll
        for (int i = 0; i < 8; i++) {
            x[i] += 1 - ((perm >> (4*i + 2)) & 2);
        }
        perm = ~perm;
    }

    return perm;
}
