#pragma once
#include "basics.h"

/**
 * Replaces x with (grad f)(x) and returns f(x), where:
 *
 * f : R^8 --> R
 *      x |--> max(0, 1 - |x|^2 / 8)^4
 *
 * is our choice of spline basis function.
 */
template<typename FloatType>
_HD_ FloatType weight_and_grad(FloatType *x) {

    FloatType r2 = 0;

    #pragma unroll
    for (int i = 0; i < 8; i++) {
        r2 += x[i] * x[i];
    }

    // compute y := min(0, |x|^2 / 8 - 1). We do this instead of
    // the more conventional max(0, 1 - |x|^2 / 8) so that
    // (grad f)(x) = y^3 x instead of -y^3 x, saving a negation.
    FloatType y = ((FloatType) 0.125) * r2 - ((FloatType) 1.0);
    if (y > ((FloatType) 0)) { y = 0; }

    FloatType y3 = y * y * y;

    #pragma unroll
    for (int i = 0; i < 8; i++) {
        // replace coordinate with gradient in-place:
        x[i] *= y3;
    }

    // f(x) = y^4 by definition.
    return y3 * y;
}
