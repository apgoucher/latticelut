#pragma once

#include <stdint.h>

#ifdef __CUDA_ARCH__
#define _HD_ __attribute__((always_inline)) __host__ __device__ inline
#else
#define _HD_ __attribute__((always_inline)) inline
#endif

typedef unsigned long long uint64_cu;
static_assert(sizeof(uint64_cu) == sizeof(uint64_t), "uint64_t should be the same as uint64_cu");

/**
 * Comparator for use in a sorting network.
 */
template<typename T>
_HD_ void inplace_maxmin(T& x, T& y) {

    T maximum = (x >= y) ? x : y;
    T minimum = (x >= y) ? y : x;
    x = maximum;
    y = minimum;

}
