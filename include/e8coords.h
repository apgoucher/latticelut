#pragma once
#include "basics.h"

/// replacement for coords_to_uint64 which uses a Clifford torus
template<typename T>
_HD_ uint64_cu coords_to_uint64_clifford(const T *c) {

    uint64_cu x = 0;

    #pragma unroll
    for (int i = 0; i < 8; i++) {
        uint64_cu y = (((int) c[i]) & 31);
        x += (y << (8*i));
    }

    // by sliding the half-integer copy of D_8, we can convert
    // the E_8 lattice into a square lattice (2Z)^8:
    if (x & 1) { x -= 0xff01010101010101ull; }

    // rearrange bits so that, when mortonised, we have bijected
    // E_8 / (32Z)^8 to the interval [0, 2^32 - 1]:
    return (x >> 1) & 0x0f0f0f0f0f0f0f0full;
}

/// helper function for coords_to_idx
template<typename T>
_HD_ uint64_cu coords_to_uint64(const T *c) {

    uint64_cu d[8];

    #pragma unroll
    for (int i = 0; i < 8; i++) {
        // map each coordinate to the range [0, 31] to avoid byte overflow
        d[i] = (((int) c[i]) & 31);
    }

    // matrix-vector multiplication within a register!
    uint64_cu idx64 = 0x40c1c18080e16060ull;
    idx64 += d[0] * 0xfdfdfbfbff030503ull;
    idx64 += d[1] * 0x0202040400fcfaffull;
    idx64 += d[2] * 0xfdfdfbfdff030501ull;
    idx64 += d[3] * 0x0202040200fcfcffull;
    idx64 += d[4] * 0xfdfdfdfdff030301ull;
    idx64 += d[5] * 0x0202020200fefcffull;
    idx64 += d[6] * 0xfdfffdfdff010301ull;
    idx64 += d[7] * 0xfdfffdfe01010301ull;

    // Now, idx64 is of the form x0 + 256 x1 + ... + 256^7 x7, where
    // each xi is in the range [0, 1023] and is divisible by 4. We
    // divide all entries by 4 (so they're in the range [0, 255]
    // and therefore occupy distinct bytes) and reduce each byte mod 8:
    idx64 = (idx64 >> 2) & 0x0707070707070707ull;

    return idx64;
}

_HD_ uint32_t mortonise(const uint64_cu &idx64) {

    uint32_t t = (uint32_t) (idx64 | (idx64 >> 28));

    { uint32_t d = (t ^ (t >>  7)) & 0x00aa00aa; t ^= d ^ (d << 7); }
    { uint32_t d = (t ^ (t >> 14)) & 0x0000cccc; t ^= d ^ (d << 14); }

    return t;
}

/**
 * Reduces a vector (c in Lambda) modulo 8 Lambda and converts into
 * an index in [0, 16777215].
 */
template<typename T>
_HD_ uint32_t coords_to_idx24(const T *c) {

    return mortonise(coords_to_uint64(c));

}

/**
 * Reduces a vector (c in Lambda) modulo 4 Lambda and converts into
 * an index in [0, 65535].
 */
template<typename T>
_HD_ uint32_t coords_to_idx16(const T *c) {

    return mortonise(coords_to_uint64(c)) & 0xffff;

}
