#pragma once

#include "basics.h"

#define _DI_ __attribute__((always_inline)) __device__ inline

#if CUDART_VERSION >= 9000
#define shuffle_32(x, y) __shfl_sync(0xffffffffu, (x), (y))
#define shuffle_xor_32(x, y) __shfl_xor_sync(0xffffffffu, (x), (y))
#define ballot_32(p) __ballot_sync(0xffffffffu, (p))
#else
#define shuffle_32(x, y) __shfl((x), (y))
#define shuffle_xor_32(x, y) __shfl_xor((x), (y))
#define ballot_32(p) __ballot(p)
#endif

/**
 * Thread-local shared memory allows efficient computed offsets
 * without spilling registers to local memory. This container uses
 * a memory layout that avoids shared memory bank conflicts.
 */
template<typename T>
struct TLSM {

    T *ptr;

    _DI_ T& operator[](int idx) {
        return ptr[idx * blockDim.x + threadIdx.x];
    }

    _DI_ T operator[](int idx) const {
        return ptr[idx * blockDim.x + threadIdx.x];
    }

    _DI_ TLSM(T* ptr) : ptr(ptr) { }

};
