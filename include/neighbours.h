#pragma once
#include <stdint.h>

/**
 * By solving convex optimisation problems (quadratic programs), it is
 * possible to prove that there are exactly 232 lattice points within a
 * distance of sqrt(8) of the _interior_ of the polytope P defined by
 * the following inequalities:
 *
 * x_0 >= x_1 >= x_2 >= x_3 >= x_4 >= x_5 >= x_6 >= |x_7|
 * x_0 + x_1 <= 2
 * x_0 + x_1 + x_2 + x_3 + x_4 + x_5 + x_6 + x_7 <= 4
 *
 * The polytope P is important because every point in R^8 can be mapped
 * into this polytope by some combination of:
 *
 * (a) translating by a lattice vector;
 * (b) permuting the coordinates;
 * (c) negating an even number of coordinates;
 *
 * all of which are isometric automorphisms of the E_8 lattice.
 *
 * Consequently, to find the points within sqrt(8) of an arbitrary point
 * in R^8, we take the transformation required to map it into P and apply
 * the _inverse_ transform to each of these 232 points.
 *
 * Each point is represented by the sum of (x_i + 4) << (4*i), to make it
 * easy to read the coordinates from the hexadecimal digits. For example,
 * 0x35555333u represents (-1, -1, -1, +1, +1, +1, +1, -1).
 */
const static uint32_t __232points[256] =
#include "lut232.inc"
