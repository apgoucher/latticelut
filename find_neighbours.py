from itertools import product

def compute_squared_distances(candidates):

    import cvxopt
    from tqdm import tqdm

    G = [([0]*i + [-1, 1] + [0]*(6-i)) for i in range(7)] + [
        [0,0,0,0,0,0,-1,-1],
        [1,1,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1]]

    h = [0,0,0,0,0,0,0,0,2,4]

    m = cvxopt.matrix
    cvxopt.solvers.options['show_progress'] = False

    res = {}

    for cv in tqdm(candidates):

        p = [-2.0*x for x in cv]
        Q = [[(2.0 if (i == j) else 0) for i in range(8)] for j in range(8)]
        sol = cvxopt.solvers.qp(m(Q), m(p), m([[1.0 * y for y in x] for x in zip(*G)]), m([1.0 * x for x in h]))
        
        sqd = sol['primal objective'] + sum([y*y for y in cv])
        
        res[tuple(cv)] = sqd

    return res


def obtain_candidates():

    from itertools import product
    candidates = list(product([-4,-2,0,2,4], repeat=8)) + list(product([-3,-1,1,3], repeat=8))
    candidates = [x for x in candidates if ((sum(x) % 4) == 0) and (sum([y*y for y in x]) <= 24)]
    return candidates


if __name__ == '__main__':

    print('Obtaining candidates...')
    x = obtain_candidates()
    print('Computing distances to fundamental region...')
    res = compute_squared_distances(x)
    within1 = [x for (x, y) in res.items() if y <= 7.9999]
    print("%d neighbours found" % len(within1))

    a = [hex(sum([(k+4) << (4*i) for (i, k) in enumerate(x)]))+'u' for x in within1]

    while (len(a) & 31):
        a.append('0')

    print('\n{\n%s\n}\n' % (',\n'.join(['    ' + ', '.join(a[8*i : 8*(i+1)]) for i in range(len(a) >> 3)])))
