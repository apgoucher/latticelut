# LatticeLUT: Continuously differentiable addressable memory

**LatticeLUT** is the differentiable analogue of _addressable memory_,
allowing deep learning architectures to load and store values into
memory in constant time.

This is introduced in [our paper](https://arxiv.org/abs/2107.03474),
_Differentiable Random Access Memory using Lattices_. **Please read this
paper before the rest of this README file.**

## How does this differ from ordinary addressable memory?

 - Instead of a discrete pointer, the memory is addressed using an
   16-dimensional vector of real numbers which is converted to a point
   on an 8-dimensional torus.

 - The memory locations in LatticeLUT do not store individual
   bytes, but instead store vectors of some predetermined size.

 - The topology of LatticeLUT is an eight-dimensional torus instead of
   a linear array.

 - The memory locations are lattice points spaced uniformly and efficiently
   in the eight-dimensional torus.

 - When the memory is addressed using an 16-dimensional vector which
   lies somewhere on the eight-dimensional torus, the returned value is
   a _smooth interpolation_ of the values at nearby lattice points.

## How to use

Firstly, ensure you have `nvcc` and `ninja` installed, along with Python 3
and the PyTorch package. The layer described in the paper can be imported
as follows:

    from latticelut import apply_lram

The function `apply_lram(query, memory_locations)` takes a GPU-resident
PyTorch tensor of dimensions (m, 16) and an integer (a power of two in the
interval $`[2^8, 2^{31}]`$ specifying the number of memory locations) and
outputs a pair of tensors:

 - an integer-typed array of shape `(m, 128)`, where each integer is in
   the interval `[0, memory_locations - 1]`, giving lookup indices;

 - a float-typed array of shape `(m, 128)` giving corresponding weights.

This is best used in conjunction with a PyTorch `EmbeddingBag` of the
same size:

    from torch.nn import EmbeddingBag
    memory_locations = 262144
    value_dim = 64
    eb = EmbeddingBag(memory_locations, value_dim, mode='sum').cuda()

Then we can perform a lookup as follows:

    # create a random query:
    import torch
    batch_size = 1000
    query = torch.randn(batch_size, 16, device='cuda', requires_grad=True)

    # perform the lookup:
    indices, weights = apply_lram(query, memory_locations)
    output = eb(indices, per_sample_weights=weights)

The output is an array of shape `(batch_size, value_dim)`.

## Geometry

The lattice $`\Lambda`$ in question is a scaled copy of the $`E_8`$ lattice,
which Maryna Viazovska proved is the densest way to pack spheres in eight-
dimensional space. A vector $`x \in \mathbb{Z}^8`$ belongs to the lattice
$`\Lambda`$ if and only if:

 - all coordinates have the same parity;
 - the sum of all eight coordinates is divisible by 4.

The minimum distance between a pair of lattice points is $`\rho = \sqrt{8}`$.
Every point in the ambient space $`\mathbb{R}^8`$ is within a distance of
at most $`\rho / \sqrt{2} = 2`$ from the nearest lattice point; the points
for which equality holds are known as _deep holes_.

Of course, the lattice contains infinitely many points, so it is impossible
to store in RAM. We instead work on the quotient $`\Lambda / L_K`$, where
$`L_K`$ is a sublattice; this quotient has $`2^n`$ points which we use as
memory locations.

![](e8emblem.png)
