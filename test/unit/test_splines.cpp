
#include "gtest/gtest.h"
#include "../../include/splines.h"

TEST(Splines, DeepHole) {

    double x[8] = {0, 0, 0, 0, 1, 1, 1, 1};

    auto v = weight_and_grad(x);

    EXPECT_EQ(v, 0.0625);

    for (int i = 0; i < 4; i++) {
        EXPECT_EQ(x[i], 0.0);
        EXPECT_EQ(x[i+4], -0.125);
    }

}

