
#include "gtest/gtest.h"
#include "../../include/e8coords.h"
#include "../../include/nearest.h"
#include "../../include/neighbours.h"
#include "../../include/splines.h"

#include <vector>
#include <iostream>
#include <algorithm>

/**
 * Integral basis for the E_8 lattice.
 *
 * The first four of these vectors are mutually orthogonal, as are
 * the last four of these vectors. They correspond to the following
 * nodes in the Coxeter-Dynkin diagram:
 *
 * 0 --- 4 --- 1 --- 5 --- 2 --- 6 --- 3
 *             |
 *             |
 *             7
 *
 * which is bipartite with vertex-classes {0,1,2,3} and {4,5,6,7}.
 *
 * This ensures that (for all n) the indices {16n, ..., 16n + 15}
 * correspond to the vertices of a regular tesseract of side-length
 * sqrt(8). Together with the Morton ordering, this results in good
 * spatial locality.
 */
const static int basis[8][8] = {{ 2,  2,  0,  0,  0,  0,  0,  0},
                                { 0,  0,  2,  2,  0,  0,  0,  0},
                                { 0,  0,  0,  0,  2,  2,  0,  0},
                                { 0,  0,  0,  0,  0,  0, -2,  2},
                                { 0,  2,  2,  0,  0,  0,  0,  0},
                                { 0,  0,  0,  2,  2,  0,  0,  0},
                                { 0,  0,  0,  0,  0,  2,  2,  0},
                                { 1, -1,  1,  1, -1,  1, -1, -1}};

TEST(E8, Morton) {
    for (int i = 0; i < 8; i++) {
        EXPECT_EQ(mortonise(1ull << (8*i)), 1 << i);
    }
}


TEST(E8, Neighbours) {

    int minimum_neighbours = 232;
    int maximum_neighbours = 0;

    int neighbour_counts[128];

    double minimum_energy_fraction = 1.0;
    double average_energy_fraction = 0.0;
    double average_neighbours = 0.0;

    for (int i = 0; i < 128; i++) {
        neighbour_counts[i] = 0;
    }

    int Npoints = 1048576;

    uint64_t state = 0x4d595df4d0f33173ull; // for PRNG

    for (int i = 0; i < Npoints; i++) {

        float z[8];

        {
            for (int j = 0; j < 8; j++) {

                // update LCG state:
                uint64_t x = state;
                state = x * 6364136223846793005ull + 1442695040888963407ull;

                // compute PCG output (uint32_t)
                uint32_t count = (uint32_t) (x >> 59);
                x ^= x >> 18;
                uint32_t w = (uint32_t) (x >> 27);
                w = (w >> count) | (w << (31 & (-count)));

                // scale to [-2, 2]:
                double coord = ((double) w) / ((double) 65536);
                z[j] = (coord - 32768.0) / 16384.0;
            }
        }

        int y[8];
        uint32_t gelem = find_closest_lattice_point(z, y);

        double total_energy = 0.0;
        int neighbour_count = 0;

        double energies[128];

        for (int j = 0; j < 232; j++) {

            int neighbour[8];
            transform_neighbour(neighbour, y, gelem, __232points[j]);

            double diff[8];

            for (int k = 0; k < 8; k++) {
                diff[k] = ((double) z[k]) - neighbour[k];
            }

            double energy = weight_and_grad(diff);

            total_energy += energy;

            if (energy > 0.0) {
                energies[neighbour_count] = energy;
                neighbour_count += 1;
            }
        }

        std::sort(energies, energies + neighbour_count);

        double energy_fraction = 0.0;
        for (int j = neighbour_count - 32; j < neighbour_count; j++) {
            energy_fraction += energies[j];
        }
        energy_fraction /= total_energy;

        if (energy_fraction < minimum_energy_fraction) { minimum_energy_fraction = energy_fraction; }
        average_energy_fraction += energy_fraction;
        average_neighbours += neighbour_count;

        if (neighbour_count < minimum_neighbours) { minimum_neighbours = neighbour_count; }
        if (neighbour_count > maximum_neighbours) { maximum_neighbours = neighbour_count; }
        neighbour_counts[neighbour_count] += 1;

        EXPECT_LE(total_energy, 1.0); // maximum = 1
        EXPECT_GE(total_energy, 0.8512221704); // minimum = (22158 - 625 Sqrt[5]) / 24389

        if ((i & 65535) == 65535) {
            std::cout << ((i >> 16) + 1) << " x 2^16 random points checked." << std::endl;
        }
    }

    average_energy_fraction /= Npoints;
    average_neighbours /= Npoints;

    EXPECT_LE(maximum_neighbours, 121);

    std::cout << "Minimum neighbours: " << minimum_neighbours << std::endl;
    std::cout << "Average neighbours: " << average_neighbours << std::endl;
    std::cout << "Maximum neighbours: " << maximum_neighbours << std::endl;

    std::cout << "Histogram: [ ";
    for (int i = minimum_neighbours; i <= maximum_neighbours; i++) {
        std::cout << neighbour_counts[i] << " ";
    }
    std::cout << "]" << std::endl;

    std::cout << "Minimum energy fraction: " << minimum_energy_fraction << std::endl;
    std::cout << "Average energy fraction: " << average_energy_fraction << std::endl;
}

TEST(E8, NearestPoint) {

    for (int i = 0; i < 16777216; i++) {

        int x[8] = {0};

        for (int j = 0; j < 8; j++) {
            int mult = ((i >> (3*j)) & 7) - 4;
            for (int k = 0; k < 8; k++) {
                x[k] += mult * basis[j][k];
            }
        }


        float z[8];

        {
            // add a random point on the sphere of radius 1.414 to the
            // lattice point. This works because 1.414 < sqrt(2), and
            // the lattice induces an efficient sphere packing of balls
            // of radius sqrt(2).
            float eps[8]; float ssq = 0.0f; uint32_t w = i;
            for (int j = 0; j < 8; j++) {
                eps[j] = (((float) (w >> 16)) - 32767.0f);
                w *= 3511;
                ssq += eps[j] * eps[j];
            }
            float scaling_factor = 1.414f / sqrtf(ssq);
            for (int j = 0; j < 8; j++) {
                z[j] = x[j] + eps[j] * scaling_factor;
            }
        }

        int y[8];
        uint32_t gelem = find_closest_lattice_point(z, y);

        int negated_coords = 0;

        for (int j = 0; j < 8; j++) {
            z[j] -= x[j];
            EXPECT_EQ(x[j], y[j]);

            if ((gelem >> (4*j)) & 8) {
                z[j] *= -1.0f;
                negated_coords += 1;
            }

            if (((gelem >> (4*j)) & 7) != 7) {
                // all coordinates except for the smallest must be
                // correctly oriented:
                EXPECT_GE(z[j], 0.0f);
            }
        }

        // check that an even number of coordinates are negated:
        EXPECT_EQ(negated_coords % 2, 0);

        // check that the coordinates are correctly permuted:
        for (int j = 1; j < 8; j++) {
            for (int k = 0; k < j; k++) {
                int ij = (gelem >> (4*j)) & 7;
                int ik = (gelem >> (4*k)) & 7;
                if (ij < ik) {
                    EXPECT_GE(z[j] + 0.000001f, z[k]);
                } else {
                    EXPECT_GE(z[k] + 0.000001f, z[j]);
                }
                EXPECT_NE(ij, ik);
            }
        }

        if ((i & 1048575) == 1048575) {
            std::cout << ((i >> 20) + 1) << " x 2^20 random points checked." << std::endl;
        }
    }
}

TEST(Lattice, Coords) {

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 32; j++) {
            int bvec[8];
            for (int k = 0; k < 8; k++) { bvec[k] = basis[i][k] * j; }
            EXPECT_EQ(((uint64_t) (j & 7)) << (i * 8), coords_to_uint64(bvec));

            if (j == 1) {
                EXPECT_EQ(coords_to_idx24(bvec), 1 << i);
            }
        }
    }
}

/**
 * Check all 2^32 E_8 lattice points lying in (Z/32Z)^8.
 */
TEST(Lattice, ExhaustiveCoords) {

    std::vector<uint64_t> nullspace;

    for (int i = 0; i < 65536; i++) {

        int coords[8];
        int sparity = 0;
        int scoords = 0;
        for (int j = 0; j < 8; j++) {
            coords[j] = (i >> (j*2)) & 3;
            sparity += coords[j] & 1;
            scoords += coords[j];
        }

        if ((sparity != 0) && (sparity != 8)) { continue; }
        if (scoords & 3) { continue; }

        uint64_t k = 0;

        for (int j = 0; j < 8; j++) {
            k += ((uint64_t) coords[j]) << (3 + (8*j));
        }

        nullspace.push_back(k);

    }

    EXPECT_EQ(nullspace.size(), 256);

    std::vector<int> coverage(65536);

    for (int i = 0; i < 16777216; i++) {

        uint64_t coords[8] = {0ull};
        uint64_t ex = 0;

        for (int j = 0; j < 8; j++) {
            uint64_t c = (i >> (3*j)) & 7;

            for (int k = 0; k < 8; k++) {
                coords[k] += c * basis[j][k];
            }

            ex += (c << (8*j));
        }

        coverage[coords_to_idx16(coords)] += 1;

        uint64_t cc = 0;
        for (int j = 0; j < 8; j++) {
            cc += (coords[j] & 31) << (8*j);
        }

        for (int j = 0; j < 256; j++) {
            uint64_t combined_coords = cc + nullspace[j];
            EXPECT_EQ(ex, coords_to_uint64((const uint8_t*) (&combined_coords)));
        }

        if ((i & 1048575) == 1048575) {
            std::cout << ((i >> 20) + 1) << " x 2^28 lattice points checked." << std::endl;
        }
    }

    for (int i = 0; i < 65536; i++) {
        EXPECT_EQ(coverage[i], 256);
    }
}
